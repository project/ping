<?php

/**
 * Ping module admin functions.
 */
function ping_settings() {
  $form = array();

  $form['ping_response'] = array(
    '#type' => 'textfield',
    '#title' => t('Response message'),
    '#default_value' => variable_get('ping_response', PING_DEFAULT_RESPONSE),
    '#description' => t('The message returned when the server is pinged.'),
    '#required' => TRUE,
  );

  $form['ping_status_code'] = array(
    '#type' => 'select',
    '#title' => t('Status code'),
    '#options' => ping_http_status(),
    '#default_value' => variable_get('ping_status_code', PING_DEFAULT_STATUS_CODE),
    '#description' => t('Optional status code to return with above content.'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
